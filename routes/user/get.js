module.exports = (router) => {
    const controller = require('../../controller/user/get')

    router.route('/user').get(controller.listar)

    router.route('/user/sort_by_name').get(controller.listarPorOrdemAlfabetica)

    router.route('/user/sort_by_suite').get(controller.listarPelaPalavraSuite)
}