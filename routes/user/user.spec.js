module.exports = (chai, server) => {
    const expect = chai.expect

     describe('User', () => {

        describe('/GET /user', () => {
            it('should list all the users', (done) => {
                chai.request(server)
                .get(`${server.APIROOTPATH}/user`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.to.be.a('object')
                    done()
                })
            })
        })

        describe('/GET /user/sort_by_name', () => {
            it('should list all the users', (done) => {
                chai.request(server)
                .get(`${server.APIROOTPATH}/user/sort_by_name`)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.to.be.a('array')
                    expect(res.body).to.be.sorted()
                    done()
                })
            })
        })

        describe('/GET /user/sort_by_suite', () => {
            it('should list all the users', (done) => {
                chai.request(server)
                .get(`${server.APIROOTPATH}/user/sort_by_suite`)
                .end((err, res) => {
                    let address = ""
                    res.should.have.status(200)
                    res.body.should.to.be.a('array')
                    res.body.forEach(elt => {
                        address = elt["address"]["street"] + elt["address"]["suite"] + elt["address"]["city"]
                        address.should.match(/[Ss]u.te/i)
                    })
                    done()
                })
            })
        })
     })
}