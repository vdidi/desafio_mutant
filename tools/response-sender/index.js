const logger = require('../logger')

const info_status = [100,101,102,103,200,201,202,203,204,205,206,207,
                     208,210,226,300,301,302,303,304,305,306,307,308,
                     310]
const error_status = [400,401,402,403,404,405,406,407,408,409,410,411,
                      412,413,414,415,416,417,418,421,422,423,424,425,
                      426,428,429,431,449,450,451,456,444,495,496,497,
                      499,500,501,502,503,504,505,506,507,508,509,510,
                      511,520,521,522,523,524,525,526,527]

const send = (res,status,content) => {
    res.status(status).send(content)

    if(process.env.NODE_ENV != "test")
        salvarInteracao(res)

    if(process.env.NODE_ENV !== "test"){
        if(info_status.includes(status)){
            logger.info(`status=${res.statusCode}, method=${res.req.method}, url=${res.req.originalUrl}, client_ip=${res.req.connection.remoteAddress}, client_userAgent=${res.req.headers['user-agent']}`)
        }else if(error_status.includes(status)){
            logger.error(`status=${res.statusCode}, method=${res.req.method}, url=${res.req.originalUrl}, client_ip=${res.req.connection.remoteAddress}, client_userAgent=${res.req.headers['user-agent']}, message=${content.message}`)
        }else{
            logger.warning(`status=${res.statusCode}, method=${res.req.method}, url=${res.req.originalUrl}, client_ip=${res.req.connection.remoteAddress}, client_userAgent=${res.req.headers['user-agent']}`)
        }
    }
}

const salvarInteracao = (res) => {
    var elasticsearch = require('elasticsearch');
    var client = new elasticsearch.Client({
    host: process.env.ELASTICSEARCH_HOST,
    });

    client.index({
        index: 'logs',
        type: 'logs_interacao',
        body: {"status": res.statusCode, 
               "method": res.req.method, 
               "url": res.req.originalUrl, 
               "client_ip": res.req.connection.remoteAddress,
               "client_userAgent": res.req.headers['user-agent'],
               "date": `${new Date()}` 
            }
    }, (err, resp, status) => {
        if(err)
            console.log(err)
        else  
            console.log(resp)
    })
}

module.exports = send