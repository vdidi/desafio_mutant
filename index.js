require('dotenv').config()
global.send = require('./tools/response-sender')
const express = require('express')
const env = process.env.NODE_ENV || "development"
const app = express()
const port = process.env.port || 8080
var elasticsearch = require('elasticsearch');

if (env != "test"){
    
    var client = new elasticsearch.Client({
        host: process.env.ELASTICSEARCH_HOST,
        //log: 'trace'
    });
    
    client.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 3000,
        //keepAlive: false
    }, function (error) {
        if (error) {
            console.trace(error);
        } else {
            console.log('Elasticsearch running');
      }
    });
}

//set the API prefix
app.APIROOTPATH = '/api'

//carrega as rotas da API
app.use(app.APIROOTPATH, require('./routes/'))

app.listen(port, () => {
    console.log('Servidor escutando na porta 8080')
})

module.exports = app