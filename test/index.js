process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')
const should = chai.should()

chai.use(chaiHttp)
chai.use(require("chai-sorted"))
chai.use(require('chai-match'))

// Import global test file
require('../routes/test.spec')(chai,server)