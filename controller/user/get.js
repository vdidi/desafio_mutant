const request = require('request')
const link =  "https://jsonplaceholder.typicode.com/users"

exports.listar = (req, res) => {

    request(link, function (error, response, body) {
        if (error)
            console.log('error:', error);

        //res.status(200).send(body)
        send(res,200, body)
    });

}

exports.listarPorOrdemAlfabetica = (req, res) => {

    request(link, function (error, response, body) {
        if (error)
            console.log('error:', error); 
        const obj = ordenarAlfabeticamente(body)
        //res.status(200).send(obj)
        send(res,200, obj)
    })

}

const ordenarAlfabeticamente = (users) => {

    let objs = JSON.parse(users)
    let sort_users = []
    objs.map(o => {
        sort_users.push(o["name"])
    })

    return sort_users.sort()
}

exports.listarPelaPalavraSuite = (req, res) => {

    request(link, function (error, response, body) {
        if (error)
            console.log('error:', error); // Print the error if one occurred
        const users = matchPalavraSuite(body)
        //res.status(200).send(users)
        send(res,200, users)
    });
}

const matchSuite = (endereco) => {
    const regExp = /[sS]u.te/
    return regExp.test(endereco)
}

const matchPalavraSuite = (users) => {
    
    let objs = JSON.parse(users)
    let sort_users = []
    let string = ''
    objs.map(user => {
        string = user["address"]["street"] + user["address"]["suite"] + user["address"]["city"]
        if(matchSuite(string))
            sort_users.push(user)
    })
    return sort_users
}